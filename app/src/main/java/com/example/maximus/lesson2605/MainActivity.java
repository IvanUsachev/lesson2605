package com.example.maximus.lesson2605;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    public static final int REQUEAT__ADD = 101;

    TextView label;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        label = (TextView) findViewById(R.id.label);

        label.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog();

            }
        });
    }

    public void showDialog() {

        final EditText input = new EditText(MainActivity.this);





        AlertDialog.Builder builder = new AlertDialog.Builder(this)
                .setTitle("Enter text")
//                .setMessage(label.getText().toString())

                .setView(input)

                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        label.setText(input.getText().toString());
                    }
                });



        builder.create().show();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (REQUEAT__ADD == requestCode && RESULT_OK == resultCode && data != null) {
            label.setText(data.getStringExtra(AddActivity.EXTRA_TEXT));
        }

        super.onActivityResult(requestCode, resultCode, data);
    }
}
